package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

public interface PurposeSubsidyMappingInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public long getPurposeOfInstallationId();

    public void setPurposeOfInstallationId(long purposeOfInstallationId);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy) ;

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);
}
