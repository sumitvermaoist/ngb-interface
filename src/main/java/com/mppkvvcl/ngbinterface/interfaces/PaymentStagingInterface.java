package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 1/2/2018.
 */
public interface PaymentStagingInterface extends BeanInterface {

    public static final boolean DELETED_TRUE = true;

    public static final boolean DELETED_FALSE = false;

    public static final String STATUS_PENDING = "PENDING";

    public static final String STATUS_MERGED = "MERGED";

    public static final String STATUS_ERROR = "ERROR";

    public long getId();

    public void setId(long id);

    public long getBillId();

    public void setBillId(long billId);

    public String getSource();

    public void setSource(String source);

    public String getTransactionId();

    public void setTransactionId(String transactionId);

    public String getModeOfPayment();

    public void setModeOfPayment(String modeOfPayment);

    public String getConsumerName();

    public void setConsumerName(String consumerName);

    public String getServiceNo();

    public void setServiceNo(String serviceNo);

    public String getIvrs() ;

    public void setIvrs(String ivrs) ;

    public String getGroupNo() ;

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public BigDecimal getAmount();

    public void setAmount(BigDecimal amount);

    public Date getDateOfTransaction();

    public void setDateOfTransaction(Date dateOfTransaction);

    public Date getDateOfVerification();

    public void setDateOfVerification(Date dateOfVerification);

    public String getErpCra();

    public void setErpCra(String erpCra);

    public long getPaymentId();

    public void setPaymentId(long paymentId) ;

    public String getStatus() ;

    public void setStatus(String status);

    public String getDescription() ;

    public void setDescription(String description) ;

    public String getCreatedBy();

    public void setCreatedBy(String createdBy) ;

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
