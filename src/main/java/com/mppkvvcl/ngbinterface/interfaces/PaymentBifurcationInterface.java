package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 1/2/2018.
 */
public interface PaymentBifurcationInterface extends BeanInterface {

    public long getId() ;

    public void setId(long id) ;

    public String getLocationCode() ;

    public void setLocationCode(String locationCode);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public long getBillId();

    public void setBillId(long billId);

    public long getPaymentId();

    public void setPaymentId(long paymentId);

    public BigDecimal getCurrentBill();

    public void setCurrentBill(BigDecimal currentBill);

    public BigDecimal getArrear();

    public void setArrear(BigDecimal arrear);

    public BigDecimal getCumulativeSurcharge();

    public void setCumulativeSurcharge(BigDecimal cumulativeSurcharge);

    public BigDecimal getAsdInstallment();

    public void setAsdInstallment(BigDecimal asdInstallment);

    public BigDecimal getAsdArrear();

    public void setAsdArrear(BigDecimal asdArrear);

    public BigDecimal getAdvancePayment();

    public void setAdvancePayment(BigDecimal advancePayment);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
