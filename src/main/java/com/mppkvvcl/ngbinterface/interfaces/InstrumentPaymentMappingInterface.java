package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface InstrumentPaymentMappingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getPaymentId();

    public void setPaymentId(long paymentId);

    public long getInstrumentDetailId();

    public void setInstrumentDetailId(long instrumentDetailId);

    public InstrumentDetailInterface getInstrumentDetail();

    public void setInstrumentDetail(InstrumentDetailInterface instrumentDetail);

    public PaymentInterface getPayment();

    public void setPayment(PaymentInterface payment);

}
