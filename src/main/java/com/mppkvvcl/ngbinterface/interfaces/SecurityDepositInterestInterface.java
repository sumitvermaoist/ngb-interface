package com.mppkvvcl.ngbinterface.interfaces;


import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 11/17/2017.
 */
public interface SecurityDepositInterestInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public Date getCalculationStartDate();

    public void setCalculationStartDate(Date calculationStartDate);

    public Date getCalculationEndDate();

    public void setCalculationEndDate(Date calculationEndDate);

    public String getBillMonth() ;

    public void setBillMonth(String billMonth);

    public BigDecimal getAmount();

    public void setAmount(BigDecimal amount);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
