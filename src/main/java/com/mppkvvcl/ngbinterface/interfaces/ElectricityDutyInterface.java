package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ElectricityDutyInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subCategoryCode);

    public long getStartConsumption();

    public void setStartConsumption(long startConsumption);

    public long getEndConsumption();

    public void setEndConsumption(long endConsumption);

    public BigDecimal getRate();

    public void setRate(BigDecimal rate);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public String getAppVersion() ;

    public void setAppVersion(String appVersion);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
