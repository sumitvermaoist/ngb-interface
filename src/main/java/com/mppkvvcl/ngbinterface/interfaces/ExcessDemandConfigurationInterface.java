package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface ExcessDemandConfigurationInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public String getValue();

    public void setValue(String value);

    public BigDecimal getMultiplier() ;

    public void setMultiplier(BigDecimal multiplier);

    public String getPenalCharge();

    public void setPenalCharge(String penalCharge);

    public BigDecimal getPenalChargeMultiplier() ;

    public void setPenalChargeMultiplier(BigDecimal penalChargeMultiplier);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy) ;

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn) ;

}
