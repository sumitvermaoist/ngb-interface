package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 11/17/2017.
 */
public interface SecurityDepositPostedInterface  extends BeanInterface {

    public long getId() ;

    public void setId(long id);

    public long getSecurityDepositId();

    public void setSecurityDepositId(long securityDepositId);

    public BigDecimal getPostedAmount();

    public void setPostedAmount(BigDecimal postedAmount);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public String getSource();

    public void setSource(String source);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public boolean isAppliedOnBill();

    public void setAppliedOnBill(boolean appliedOnBill);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
