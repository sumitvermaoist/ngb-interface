package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AdjustmentRangeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public int getCode();

    public void setCode(int code);

    public BigDecimal getStart();

    public void setStart(BigDecimal start);

    public BigDecimal getEnd();

    public void setEnd(BigDecimal end);

    public int getMaxPriority();

    public void setMaxPriority(int maxPriority);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
